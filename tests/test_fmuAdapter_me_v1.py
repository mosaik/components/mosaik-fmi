import pytest
import os
import sys
sys.path.insert(0, "..")
from mosaik_fmi.mosaik_fmi import FmuAdapter

def test_fmuAdapter_BouncingBall_me(fmi_type="me",fmu_dir="tests/data/Reference_FMU/v1/me"):
    work_dir = os.path.join(os.getcwd(), fmu_dir)
    fmu_name = "BouncingBall"
    model_id = "fmu_model"
    simulator = FmuAdapter()
    model_instance = "fmu_instance"
    reply_init = simulator.init('FMU', work_dir=work_dir, fmu_name=fmu_name, fmi_type=fmi_type)
    assert reply_init['models'] == {model_id: {'public': True, 'params': [], 'attrs': ['h', 'v']}}
    assert reply_init['extra_methods'] == ['fmi_set', 'fmi_get']
    reply_create = simulator.create(1, model_instance)
    assert reply_create == [
        {'eid': model_instance + '_0', 'type': model_instance, 'rel': []}
    ]

    reply_step = simulator.step(0, {model_instance+'_0': {}})
    assert reply_step < 0.5

    reply_get_data = simulator.get_data({model_instance + '_0': ['h']})
    assert reply_get_data['fmu_instance_0']['h'] < 0.05

    simulator.set_values(model_instance+'_0', {'h': 1}, var_type='output')
    value = simulator.get_value(model_instance+'_0', 'h')
    assert value == 1

    simulator.set_values(model_instance + '_0', {'h': 1,'v':0}, var_type='output')
    value = simulator.get_value(model_instance + '_0', 'h')
    assert value == 1

    reply_step = simulator.step(1, {model_instance + '_0': {}})
    assert reply_step < 1.5

    reply_get_data = simulator.get_data({model_instance + '_0': ['h']})
    assert reply_get_data['fmu_instance_0']['h'] < 0.05
