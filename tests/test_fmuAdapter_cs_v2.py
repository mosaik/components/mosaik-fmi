import pytest

import os
import sys
sys.path.insert(0, "..")
from mosaik_fmi.mosaik_fmi import FmuAdapter


def test_fmuAdapter_BouncingBall_cs(fmi_type="cs",fmu_dir="tests/data/Reference_FMU/v2"):
    work_dir = os.path.join(os.getcwd(), fmu_dir)
    fmu_name = "BouncingBall"
    model_id = "fmu_model"
    simulator = FmuAdapter()
    model_instance = "fmu_instance"
    reply_init = simulator.init('FMU', work_dir=work_dir, fmu_name=fmu_name, fmi_type=fmi_type)

    assert reply_init['models'] == {model_id: {'public': True, 'params': ['g', 'e'], 'attrs': ['g', 'e', 'h', 'v']}}
    assert reply_init['extra_methods'] == ['fmi_set', 'fmi_get']

    reply_create = simulator.create(1, model_instance)
    assert reply_create == [
        {'eid': model_instance + '_0', 'type': model_instance, 'rel': []}
    ]

    reply_step = simulator.step(0, {model_instance+'_0': {}})
    assert reply_step == 1

    reply_get_data = simulator.get_data({model_instance + '_0': ['h']})
    assert reply_get_data == {'fmu_instance_0': {'h': 0.23664368699999475}}

    simulator.set_values(model_instance+'_0', {'h': 1}, var_type='output')
    value = simulator.get_value(model_instance+'_0', 'h')
    assert value == 1

    simulator.set_values(model_instance + '_0', {'h': 1,'v':0}, var_type='output')
    value = simulator.get_value(model_instance + '_0', 'h')
    assert value == 1

    reply_step = simulator.step(1, {model_instance + '_0': {}})
    assert reply_step == 2

    reply_get_data = simulator.get_data({model_instance + '_0': ['h']})
    assert reply_get_data == {'fmu_instance_0': {'h': 0.23664368699999475}}

def test_fmuAdapter_Stair_cs(fmi_type="cs",fmu_dir="tests/data/Reference_FMU/v2"):
    work_dir = os.path.join(os.getcwd(), fmu_dir)
    fmu_name = "Stair"
    model_id = "fmu_model"
    simulator = FmuAdapter()
    model_instance = "fmu_instance"
    reply_init = simulator.init('FMU', work_dir=work_dir, fmu_name=fmu_name, fmi_type=fmi_type)

    assert reply_init['models'] == {model_id: {'public': True, 'params': [], 'attrs': ['counter']}}
    assert reply_init['extra_methods'] == ['fmi_set', 'fmi_get']
    reply_create = simulator.create(1, model_instance)
    assert reply_create == [
        {'eid': model_instance + '_0', 'type': model_instance, 'rel': []}
    ]

    reply_get_data = simulator.get_data({model_instance + '_0': ['counter']})
    assert reply_get_data == {'fmu_instance_0': {'counter': 1}}

    reply_step = simulator.step(0, {model_instance+'_0': {}})
    assert reply_step == 1

    reply_get_data = simulator.get_data({model_instance + '_0': ['counter']})
    assert reply_get_data == {'fmu_instance_0': {'counter': 2}}

    reply_step = simulator.step(1, {model_instance + '_0': {}})
    assert reply_step == 2

    reply_get_data = simulator.get_data({model_instance + '_0': ['counter']})
    assert reply_get_data == {'fmu_instance_0': {'counter': 3}}
#
def test_fmuAdapter_Dahlquist_cs(fmi_type="cs",fmu_dir="tests/data/Reference_FMU/v2"):
    work_dir = os.path.join(os.getcwd(), fmu_dir)
    fmu_name = "Dahlquist"
    model_id = "fmu_model"
    simulator = FmuAdapter()
    model_instance = "fmu_instance"
    reply_init = simulator.init('FMU', work_dir=work_dir, fmu_name=fmu_name, fmi_type=fmi_type)

    assert reply_init['models'] == {model_id: {'public': True, 'params': ['k'], 'attrs': ['k', 'x']}}
    assert reply_init['extra_methods'] == ['fmi_set', 'fmi_get']

    reply_create = simulator.create(1, model_instance)
    assert reply_create == [
        {'eid': model_instance + '_0', 'type': model_instance, 'rel': []}
    ]

    reply_step = simulator.step(0, {model_instance+'_0': {}})
    assert reply_step == 1

    reply_get_data = simulator.get_data({model_instance + '_0': ['x']})
    assert reply_get_data == {'fmu_instance_0': {'x': 0.3486784401}}

    reply_step = simulator.step(1, {model_instance + '_0': {}})
    assert reply_step == 2

    reply_get_data = simulator.get_data({model_instance + '_0': ['x']})
    assert reply_get_data == {'fmu_instance_0': {'x': 0.12157665459056928}}

    simulator.set_values(model_instance+'_0', {'x': 2.5}, var_type='output')
    value = simulator.get_value(model_instance+'_0', 'x')
    assert value == 2.5

